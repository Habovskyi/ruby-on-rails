# frozen_string_literal: true

# == Schema Information
#
# Table name: actors
#
#  id               :bigint           not null, primary key
#  description      :text
#  full_name        :string
#  gender           :string
#  is_married       :boolean          default(FALSE)
#  number_of_oscars :integer          default(0)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

# TODO: add more unit tests for model
describe Actor, type: :model do

  context 'validations' do
    it { is_expected.to validate_presence_of(:full_name) }
    it { is_expected.to have_one_attached(:image) }

    it 'validates full_name length' do
      expect(subject)
        .to validate_length_of(:full_name)
        .is_at_least(described_class::MIN_FULL_NAME_LENGTH)
        .is_at_most(described_class::MAX_FULL_NAME_LENGTH)
    end

    it 'validates description length' do
      expect(subject)
        .to validate_length_of(:description)
        .is_at_least(described_class::MIN_DESC_LENGTH)
        .is_at_most(described_class::MAX_DESC_LENGTH)
    end
  end
end
