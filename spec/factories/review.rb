# frozen_string_literal: true

FactoryBot.define do
  factory :review do
    text { Faker::Lorem.paragraph_by_chars(number: Review::MAX_TEXT_LENGTH) }
    username { Faker::Lorem.paragraph_by_chars(number: 20) }
    film
  end
end
