# frozen_string_literal: true

def image_dir
  @image_dir ||= Dir['public/image/**']
end

def create_actor
  actor = Actor.create!(
    full_name:        Faker::Name.name,
    description:      Faker::Lorem.paragraph_by_chars(number: 25),
    number_of_oscars: rand(11),
    gender:           Actor.genders.values.sample,
    is_married:       rand(0..1).zero?
  )
  actor.image.attach(io: File.open(image_dir.sample), filename: "#{actor.full_name}.jpg")
end

Rails.logger.info '#=== Populating actors ===#'
50.times { create_actor }
Rails.logger.info '#=== Populating actors finished ===#'
