class CreateActorFilms < ActiveRecord::Migration[6.1]
  def change
    create_table :actor_films do |t|
      t.belongs_to :actor
      t.belongs_to :film
      t.timestamps
    end
  end
end
