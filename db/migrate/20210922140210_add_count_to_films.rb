class AddCountToFilms < ActiveRecord::Migration[6.1]
  def change
    add_column :films, :count, :integer, default: 0
  end
end
