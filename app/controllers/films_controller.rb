# frozen_string_literal: true

class FilmsController < ApplicationController
  before_action :find_film!, only: %i[show destroy]
  before_action :authenticate_user!

  def index
    @films         = Film.all
    @pages, @films = pagy(Film.includes(:photo_blob).order(created_at: :desc))
  end

  def show
    @films      = Film.all
    @film.increment!(:count)
    @review     = Review.new
    @film_actor = @film.actor_films.build
  end

  def new
    @actors2    = Actor.all
    @film       = Film.new
    @all_films  = Film.all
    @film_actor = @film.actor_films.build
  end

  def create
    @film = Film.create(film_params)
    params[:actors][:id].each do |film|
      @film.actor_films.build(actor_id: film) unless film.empty?
    end

    params[:actors][:id].each do |actor_id2|
      if actor_id2.to_i > 0
        ActorFilm.create(actor_id: actor_id2.to_i, film_id: @film.id)
      end
    end

    return render :new if @film.errors.any?

    redirect_to films_path, notice: I18n.t('films.create.success')
  end

  # JSON response example
  def destroy
    @film.destroy!

    respond_to do |format|
      format.html { redirect_to films_path, notice: I18n.t('films.destroy.message') }
      format.json do
        render json: { data: { message: I18n.t('films.destroy.json_message', name: @film.name) } }
      end
    end
  end

  private

  def find_film!
    @film = Film.find(params[:id])
  end

  def film_params
    params.require(:film).permit(:name, :description)
  end

  def actorfilm_params
    params.require(:actors).permit(:id, :film_id)
  end

end
