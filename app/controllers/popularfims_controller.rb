class PopularfimsController < ApplicationController
  before_action :authenticate_user!

  def popularfilm
    @pagy, @films = pagy(Film.includes(:reviews).joins(:reviews).order("count DESC"))
  end
end