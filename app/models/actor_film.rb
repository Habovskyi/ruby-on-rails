# frozen_string_literal: true

class ActorFilm < ApplicationRecord
  belongs_to :film
  belongs_to :actor
end
