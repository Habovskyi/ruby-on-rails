# frozen_string_literal: true

# == Schema Information
#
# Table name: actors
#
#  id               :bigint           not null, primary key
#  description      :text
#  full_name        :string
#  gender           :string
#  is_married       :boolean          default(FALSE)
#  number_of_oscars :integer          default(0)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class Actor < ApplicationRecord
  MIN_FULL_NAME_LENGTH = 3
  MAX_FULL_NAME_LENGTH = 25
  MIN_DESC_LENGTH      = 10
  MAX_DESC_LENGTH      = 250

  has_many :actor_films
  has_many :films, through: :actor_films

  has_one_attached :image

  validates :full_name, :description, :gender, presence: true

  validates :full_name,
            length: { minimum: MIN_FULL_NAME_LENGTH, maximum: MAX_FULL_NAME_LENGTH }
  validates :description,
            length: { minimum: MIN_DESC_LENGTH, maximum: MAX_DESC_LENGTH }

  enum gender: {
    male:   MALE   = 'male',
    female: FEMALE = 'female',
    other:  OTHER  = 'other'
  }
end
